package sorts

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestInsertion_test_for_ascending(t *testing.T){
	input2 := []int{}
	input1 := []int{5,4,2,3,1}
	input3 := []int{15,44,22,34,12}

	result1 := InsertionSort(input1, asc)
	result2 := InsertionSort(input2, asc)
	result3 := InsertionSort(input3, asc)
	expectedResult1 := []int{1,2,3,4,5}
	expectedResult2 := []int{}
	expectedResult3 := []int{12,15,22,34,44}

	assert.Equal(t, expectedResult1, result1)
	assert.Equal(t, expectedResult2, result2)
	assert.Equal(t, expectedResult3, result3)
}

func TestInsertion_test_for_desc(t *testing.T){
	input2 := []int{}
	input1 := []int{5,4,2,3,1}
	input3 := []int{15,44,22,34,12}

	result1 := InsertionSort(input1, desc)
	result2 := InsertionSort(input2, desc)
	result3 := InsertionSort(input3, desc)
	expectedResult1 := []int{5,4,3,2,1}
	expectedResult2 := []int{}
	expectedResult3 := []int{44,34,22,15,12}

	assert.Equal(t, expectedResult1, result1)
	assert.Equal(t, expectedResult2, result2)
	assert.Equal(t, expectedResult3, result3)
}