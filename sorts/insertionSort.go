package sorts

func asc(first int, second int) bool{
	return first < second
}

func desc(first int, second int) bool{
	return first > second
}

func InsertionSort(input []int, operation func(int, int)bool )[]int{
	for i:=1; i<len(input); i++  {

		currentValue := input[i]
		j := i - 1

		for operation(currentValue,input[j]) {
			next := input[j]
			input[j] = currentValue
			input[j+1] = next
			if j-1 < 0{
				break
			}
			j = j - 1
		}

	}
	return input
}